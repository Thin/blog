<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public function beforeFilter() {
    	//$this->Auth->fields = array('username' => 'username', 'password' => 'password');
        parent::beforeFilter();
        $this->Auth->allow('add','logout','login');
        
    }
   

//if($this->Auth->fields = array('username'=>'username','password'=>'password'));
public function login() {
    if ($this->request->is('post')) {
    	
        if ($this->Auth->login()) {
        	//if($this->Auth->fields = array('username'=>'username','password'=>'password'))
//{
           //$this->redirect(array('controller' => 'posts', 'actions' => 'index'));
        	$this->redirect($this->Auth->redirectUrl());
        }
         
        else{
        	 $this->Flash->error(__('Invalid username or password, try again'));
        }
    }                                                                                                                                                                                 
}
	
	public function logout() {
		//session_destroy();
		$this->redirect($this->Auth->logout());
		 $this->Session->destroy('User'); 
        $this->Session->setFlash('You\'ve successfully logged out.');
        var_export($this->Session->read('User'));
	}

    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        }
    }

    public function edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        // Prior to 2.5 use
         //$this->request->onlyAllow('post');

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

}