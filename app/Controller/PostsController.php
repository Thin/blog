<?php

class PostsController extends AppController {

    public $helpers = array('Html', 'Form', 'Flash','Session');
    public $components = array('Flash');
    var $name = 'Posts';
    var $uses = array('Post','Comment');

	public function index() {
    	 $this->paginate = array('limit'=>3,'order' => array('id' => 'asc'));
         $this->set('posts', $this->paginate('Post')); 
         $title = array();

     	 // filter by id
        if(isset($this->passedArgs['Search.id'])) {
            $id = $this ->passedArgs['Search.id'];
         	$this ->paginate = array('conditions' => array('Post.id LIKE' => $id));
         	$posts =  $this -> paginate('Post');
         	$this -> set('posts',$posts);
         	$this ->request ->data['Search']['id'] = $id;
            $title[] = __('ID',true).':'.$this ->passedArgs['Search.id'];
        }
        


      //  filter by title
        if(isset($this->passedArgs['Search.title'])) {
            $title = $this ->passedArgs['Search.title'];
        	$this ->paginate = array('conditions' => array( 'Post.title LIKE' => '%' . $title . '%'));
         	$posts =  $this -> paginate('Post');
         	$this -> set('posts',$posts);
         	$this->request ->data['Search']['title'] = $title;
            $titlesss[] = __('TITLE',true).': '.$this->passedArgs['Search.title'];
        }
    }


    public function view($id = null) {
        if (!$id) {
            $this->Flash->error(__('Invalid Comment.', 'true'),'error');
            $this->redirect(array('action'=>'index'));
        }

        // save the comment
        if (!empty($this->request->data['Comment'])) {
            $this->request->data['Comment']['class'] = 'Post'; 
            $this->request ->data['Comment']['foreign_id'] = $id; 
            $this->request->data['Comment']['name']=$this->Auth->user('username');
            $this->Post->Comment->create(); 

            if(!empty($this->request->data))
            {
                if(!empty($this->request->data['Comment']['imgcom']['name']))
                {
                    $file = $this->request->data['Comment']['imgcom']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); 
                    if(in_array($ext, $arr_ext))
                    {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);                    
                        $this->request->data['Comment']['imgcom'] = "/img/" . $file['name'];
                        pr($this->request->data);                        
                    }
                }     
            }

            if ($this->Post->Comment->save($this->request->data)) {
                $this->Flash->success(__('The Comment has been saved.'));
                $this->redirect(array('action'=>'view',$id));
                $this->request->data['Comment']=NULL;
            }
           $this->Flash->error(__('The Comment could not be saved. Please, try again.'));
        }

        // set the view variables
        $post = $this->Post->read(null, $id); // contains $post['Comments']
        $this->set('post',$post);
    }

   
    public function add() {
        
   		 if ($this->request->is('post')) 
        {
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->Post->create();
           
            if(!empty($this->request->data))
            {
                if(!empty($this->request->data['Post']['image_url']['name']))
                {
                    $file = $this->request->data['Post']['image_url']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); 
                    if(in_array($ext, $arr_ext))
                    {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);                    
                        $this->request->data['Post']['image_url'] = "/img/" . $file['name'];
                        pr($this->request->data);                        
                    }
                }     
            }
             if ($this->Post->save($this->request->data)) 
            {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            else
            {
                $this->Flash->error(__('The data could not be saved. Please, Choose your image.'));
            }
        }
	}

    

    public function edit($id = null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid Topic'));
    }

    $post = $this->Post->findById($id);
    if (!$post) {
        throw new NotFoundException(__('Invalid Topic'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Post->id = $id;

       
        if ($this->Post->save($this->request->data)) {

            $this->Flash->success(__('Your topic has been updated.'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('Unable to update your topic.'));
    }

    if (!$this->request->data) {
        $this->request->data = $post;
    }
	}

	public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }
   
    if ($this->Post->delete($id)) {
        $this->Flash->success(
            __('The topic with id: %s has been deleted.', h($id))
        );
    } 

    return $this->redirect(array('action' => 'index'));
	}

	
	public function isAuthorized($user) {
    // All registered users can add posts
         if ($this->action === 'add') {
              return true;
         }

             // The owner of a post can edit and delete it
         if (in_array($this->action, array('edit', 'delete'))) {
              $postId = (int) $this->request->params['pass'][0];
              if ($this->Post->isOwnedBy($postId, $user['id'])) {
                    return true;
               }
          }

         return parent::isAuthorized($user);
    }

	public function search() {
		$url['action'] = 'index';
		foreach ($this ->data as $k => $v){
			foreach ($v as $kk => $vv){
				if ($vv) {
					$url[$k. '.' .$kk]=$vv;
				}
			}
		}
		$this->redirect($url, null, true);
		//return $this->redirect(array('controller' => 'posts' , 'action' => 'index'));
		// $this->redirect( array('controller' => 'Invoices','action' => 'add'));
	}

    public function deletecom($id=null,$foreign_id=null) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }
   
    if ($this->Comment->delete($id)) {

    return $this->redirect(array('controller' => 'posts','action' => 'view',$foreign_id));
    }
    }   

   public function editcom($id=null,$foreign_id=null) 
    {
    $this->Comment->id = $id;
   
    $this->Post->id = $foreign_id;
   

     if ($this->request->is('get')) 
     {
        $this->request->data=$this->Comment->read();
     }
    
     else
     {
          if(!empty($this->request->data))
            {
                if(!empty($this->request->data['Comment']['imgcom']['name']))
                {
                    $file = $this->request->data['Comment']['imgcom']; 
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); 
                    if(in_array($ext, $arr_ext))
                    {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);                    
                        $this->request->data['Comment']['imgcom'] = "/img/" . $file['name'];
                        pr($this->request->data);                        
                    }
                }     
            }

         if ($this->Comment->save($this->request->data)) 
         {
            $this->redirect(array('controller' => 'posts','action' => 'view',$foreign_id));
         }

         else
         {
            $this->Flash->error(__('Unable to update your comment'));
        }

     }
}

}

?>