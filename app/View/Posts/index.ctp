<h1><b>Topic Blog</b></h1>
<?php //if(AuthComponent::user('role') == 'admin') echo 'Admin : '.AuthComponent::user('role');

echo $this->Html->link('Add Topics',array('controller' => 'posts', 'action' => 'add')); 

echo ' | ';

echo $this->Html->link('Log Out',array('controller' => 'users', 'action' => 'logout')); ?>
<br>
<br>

<?php //if(AuthComponent::user('id')) echo AuthComponent::user('username'); ?>

<table>
    <tr>
        <th>Id</th>
        <th>Image</th>
        <th>Title</th>
        <th>Action</th>
        <th>Created</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

     <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>

        <td><?php echo $this->Html->image($post['Post']['image_url'],array('width' => '100px', 'high' => '100px')); ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('controller'=>'posts','action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
       
            <?php if(($_SESSION['Auth']['User']['id'] == $post['Post']['user_id']) || (AuthComponent::user('role') == 'admin'))
          
            {
                echo $this->Form->postLink(
                    'Delete',array('action' => 'delete', $post['Post']['id']),array('confirm' => 'Are you sure?'));
                echo '   ';
           
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $post['Post']['id'])
                );
            }
            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($post); ?> 
</table>

<?php
$paginator = $this->Paginator;
// pagination section
    echo "<div class='paging'>";
 
        // the 'first' page button
        echo $paginator->first("First");
         
        // 'prev' page button, 
        // we can check using the paginator hasPrev() method if there's a previous page
        // save with the 'next' page button
        if($paginator->hasPrev()){
            echo $paginator->prev("Prev");
        }
         
        // the 'number' page buttons
        echo $paginator->numbers(array('modulus' => 2));
         
        // for the 'next' button
        if($paginator->hasNext()){
            echo $paginator->next("Next");
        }
         
        // the 'last' page button
        echo $paginator->last("Last");
     
    echo "</div>";
?>

<?php echo $this->Form->Create('Post',array('url'=>'search')); ?>

<br>

    <fieldset>
        <legend><?php echo __('Topic Search', true);?></legend>
           <?php

        echo $this->form->input('Search.id');
        echo $this->form->input('Search.title');
        echo $this->form->submit('Search');
       
    ?>
    </fieldset>
<?php echo $this->Form->end(); ?>
<?php $this->paginator->options(array('url'=>$this->passedArgs)); ?>