<?php

class Comment extends AppModel {
    var $name = 'Comment';
    public $validate = array('body'=>array('rule' => 'notBlank'));
}